Rules

The game uses the same cards as Binmat, and some to similar use

Turns are non-linear. Both players get a turn every 5 seconds and, as long as they have a turn, can play at any time. If both players play at once, the player with less turns takes priority.

Both players have 2 lanes, Attack and Defend. Once a card is played to a lane, it can not be moved, and only discarded once destroyed or otherwise used.

-=-= Modifier Rules =-=-

If a (@)TRAP card is interacted with in the defending lane, any remaining damage is mitigated back to the other players defending lane.
If a (@)TRAP card is interacted with in the attacking lane, a ?BOUNCE card may be ignored.

If a (\*)WILD card is interacted with in the defending lane, it's value becomes the top card's value of the opponents defending lane
If a (\*)WILD card is interacted with in the attacking lane, it's value becomes the top card's value of the opponents attacking lane

If a (?)BOUNCE card is interacted with in the defending lane, all attacks become 0 until the attacker runs out of cards ready to attack, or a (@)TRAP card is used by the attacker. Discard at the end of combat.
IF a (?)BOUNCE card is interacted with in the attacking lane, then combat ends. Discard at the end of combat

If a (>)BREAK card is interacted with in the defending lane, discard all cards of a suit in the opponents defending lane.
If a (>)BREAK card is interacted with in the attacking lane, discard all cards of a suit in the opponents attacking lane.

-=-= COMBAT =-=-

Both players start the game with 20GC.

When a player declares attack, they start playing from their attacking lane, top to bottom, and the defender plays from their defending lane, top to bottom. Defender takes priority and interacts first. If the cards interacted with are modifiers, apply their effects. If it is a number card, add that to the owner's power, then discard the card. Each interaction compare either side's power. If the defender has higher power, both sides go through interaction again. If the attacker has higher power, the overflow is applied as damage to the defender, and both side's power becomes 0 again. If it is a tie, overcharge occurs and both players shuffle their respective lanes. If they tie again during combat, blackout occurs and both players discard their respective lanes without interacting. Combat concludes.

If the attacker's attacking lane becomes empty, Combat concludes. If the defender's lane becomes empty, they interact with a power of 0.

At the end of combat, all power is discarded.

Example:

Player 1 plays 3

Player 2 plays 5

Player 1 plays 9

Player 2 plays 8

Player 2 declares attack

Player 1 interacts with 9 (Power(0) + 9) (9)

Player 2 interacts with 8 (Power(0) + 8)

Breach occurs. 8 !> 9, attacker(Player 2) fails, reinteraction occurs

Player 1 interacts with 3 (Power(9) + 3) (12)

Player 2 interacts with 5 (Power(8) + 5) (13)

Breach occurs. 13 > 12, attacker(Player2) wins, Player 1 takes 13-12 damage.

Attacker Lane Empty, combat concluded.