require 'gosu'

def link(suit)
  puts "Linking #{suit}...".upcase
  return "./textures/cards/" + suit.upcase + ".png"
end

class Game < Gosu::Window
  def initialize()
    puts ">>>LINKING SUITS"
    @CHAOS  = Gosu::Image.new(link("chaos"),options = {retro:true})
    @CHOICE = Gosu::Image.new(link("choice"),options = {retro:true})
    @DATA   = Gosu::Image.new(link("data"),options = {retro:true})
    @FORM   = Gosu::Image.new(link("form"),options = {retro:true})
    @KIN    = Gosu::Image.new(link("kin"),options = {retro:true})
    @VOID   = Gosu::Image.new(link("void"),options = {retro:true})
    puts ">>>SUITS LINKED."
    puts ">>>LINKING UI".upcase
    puts "LINKING PLAY"
    @PLAY   = Gosu::Image.new("textures/ui/Play.png")
    puts ">>>UI LINKED"
    puts ">>>Linking FONT".upcase
    @TEXT   = "fonts/pixelated.ttf"
    puts ">>>FONT Linked.".upcase
    puts ">>>Linking VARIABLES".upcase
    @CARDS  = ["@","*","?",">","2","3","4","5","6","7","8","9","10"]
    @PLAYER = ["/","/","/","/","/"]
    @AI     = ["/","/","/","/","/"]
    @SCENE  = 0
    puts "VARIABLES LINKED.".upcase
    @res_x = 1280
    @res_y = 720
    super @res_x, @res_y
  end

  def update()
    if @SCENE == 0
      if mouse_x.between?(0, 300)
        if mouse_y.between?(0, 200)
          if Gosu.button_down? Gosu::MS_LEFT
            @SCENE == 1
          end
        end
      end
    end
  end

  def draw()
    @CHAOS.draw_rot(@res_x/2,@res_y/2,0,0,0.5,0.5,0.3,0.3)
    Gosu::Image.from_text("/", 200, {font:@TEXT,retro:true}).draw_rot(@res_x/2,@res_y/2,1,0,0.5,0.5)
  end
end

Game.new.show
